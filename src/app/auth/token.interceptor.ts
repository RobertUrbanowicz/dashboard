import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestClone = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.authService.getToken()}`,
      }
    });
    console.log('http token interceptor');

    return next.handle(requestClone);
  }
  
}