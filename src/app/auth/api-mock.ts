import { AUTHENTICATION_TIME } from './../../constants/main.constants';
import { HmacSHA256 } from 'crypto-js';

export interface LoginSuccessResponse {
  token: string;
  user: string;
}

class API {
  private generateToken(user: string) {
    const header = JSON.stringify({
        "alg": "HmacSHA256",
        "typ": "JWT",
      });
      const payload = JSON.stringify({
        "sub": user,
        "exp": Date.now() + AUTHENTICATION_TIME, // 10 sec
    });

    const headerBase64 = btoa(header);
    const payloadBase64 = btoa(payload);
    const signature = HmacSHA256(`${headerBase64}.${payloadBase64}`, 'secret');
    const token = `${headerBase64}.${payloadBase64}.${signature}`;

    return token;
  }

  login (user: string, passwd: string): LoginSuccessResponse {
    const token = this.generateToken(user);
    return {
      token,
      user,
    }
  }
}

export default new API();
