import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import API, { LoginSuccessResponse } from './api-mock';

const TOKEN_STORAGE_KEY = 'auth_token';
const TOKEN_EXPIRATION_STORAGE_KEY = 'auth_token_expiration';


@Injectable()
export class AuthService {

  getToken() {
    return localStorage.getItem(TOKEN_STORAGE_KEY);
  }

  private setToken(token: string) {
    localStorage.setItem(TOKEN_STORAGE_KEY, token);
  }

  getTokenExpiration() {
    const exp = localStorage.getItem(TOKEN_EXPIRATION_STORAGE_KEY);
    return +exp;
  }

  private setTokenExpiration() {
    const token = this.getToken();
    const payloadBase64 = token.split('.')[1];
    const payloadStr = atob(payloadBase64);
    const payload = JSON.parse(payloadStr);

    console.log('Token exp:', payload.exp);
    localStorage.setItem(TOKEN_EXPIRATION_STORAGE_KEY, payload.exp);
  }

  isAuthenticated() {
    const token = this.getToken();
    const tokenExp = this.getTokenExpiration();
    
    if (!token || !tokenExp) return false;
    return tokenExp > Date.now();
  }

  login(user: string, passwd: string) {
    return new Observable<LoginSuccessResponse>((obs) => {
      const response = API.login(user, passwd);
      this.setToken(response.token);
      this.setTokenExpiration();
      obs.next(response);
      obs.complete();
    });
  }

  logout() {
    localStorage.removeItem(TOKEN_STORAGE_KEY);
    localStorage.removeItem(TOKEN_EXPIRATION_STORAGE_KEY);
  }
}
