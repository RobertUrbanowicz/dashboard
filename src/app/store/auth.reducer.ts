import {  } from "@ngrx/store";
import * as actions from "./auth.action";

export interface AuthState {
  user: string | null;
  token: string | null;
  authenticated: boolean;
}

export const initialState: AuthState = {
  user: null,
  token: null,
  authenticated: false,
}

export function reducer(state = initialState, action: actions.AuthAction): AuthState {
  switch (action.type) {
    case actions.LOGIN_SUCCESS: {
      return {
        authenticated: true,
        ...(action as actions.LoginSuccess).payload,
      }
    }

    case actions.LOGOUT: {
      return {
        user: null,
        token: null,
        authenticated: false,
      }
    }

    default: return state;
  }
}
