import { Action } from '@ngrx/store';
import { LoginSuccessResponse } from '../auth/api-mock';


export const LOGIN = 'Login';
export const LOGOUT = 'Logout';
export const LOGIN_SUCCESS = 'Login Success';
export const LOGIN_FAIL = 'Login Fail';


export class Login implements Action {
  readonly type = LOGIN;
  constructor(public user: string, public password: string) {}
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;
  constructor(public payload: LoginSuccessResponse) {}
}

export class LoginFail implements Action {
  readonly type = LOGIN_FAIL;
  constructor(public payload: any) {}
}

export type AuthAction = Login | Logout | LoginSuccess | LoginFail;
