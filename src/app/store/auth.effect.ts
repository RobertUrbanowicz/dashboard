import { Router } from '@angular/router';
import { LoginSuccessResponse } from './../auth/api-mock';
import { Login, LoginSuccess, LoginFail, LOGIN, LOGIN_SUCCESS, LOGOUT } from './auth.action';
import { AuthService } from './../auth/auth.service';
import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class AuthEffect {
  constructor(private actions$: Actions, private authService: AuthService, private router: Router) {}

  @Effect()
  login$ = this.actions$.pipe(ofType(LOGIN),
    exhaustMap((action: Login) => this.authService.login(action.user, action.password).pipe(
      map((resp: LoginSuccessResponse) => new LoginSuccess(resp)),
      catchError(err => of(new LoginFail(err)))
    )),
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(ofType(LOGIN_SUCCESS),
    map((action: LoginSuccess) => {
      this.router.navigate(['dashboard']);
    }),
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(ofType(LOGOUT),
    map(() => {
      this.authService.logout();
      this.router.navigate(['login']);
    }),
  );

}