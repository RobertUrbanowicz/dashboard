import { Login } from './../../store/auth.action';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Store } from '@ngrx/store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private store: Store, private http: HttpClient) { }

  ngOnInit(): void {
  }

  login(user: string, password: string) {
    if (!user || !password)
      alert('Please fill user and password');
    else
      this.store.dispatch(new Login(user, password));
  }

  ping() {
    this.http.get('/foo').subscribe();
  }
}
