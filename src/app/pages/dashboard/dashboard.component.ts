import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Logout } from 'src/app/store/auth.action';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private store: Store, private http: HttpClient) { }

  ngOnInit(): void {
  }

  logout() {
    this.store.dispatch(new Logout());
  }

  ping() {
    this.http.get('/foo').subscribe();
  }

}
