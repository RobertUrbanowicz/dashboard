import { AuthState } from './store/auth.reducer';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  authenticated$: Observable<boolean>;

  constructor(private store: Store) {}

  ngOnInit() {
    this.authenticated$ = this.store.select((state: { auth: AuthState }) => {
      return state.auth.authenticated;
    });
  }
}
